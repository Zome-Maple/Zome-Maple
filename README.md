# Zome Maple

**NB:** The code in this repository is under development for the time being.
As a result, the interface of the library is subject to change.
Additionally, outputs are not guaranteed to be correct, and things may break occasionally.

A paper describing this package was presented at the Waterloo Maple Conference 2019.
The functionality and rationale are described in the paper, which can be found on the [Arxiv](https://arxiv.org/abs/1908.07153) and on the [publisher's website](https://doi.org/10.1007/978-3-030-41258-6_5):

- Benoit Charbonneau and Spencer Whitehead, *Studying Wythoff and Zometool Constructions using Maple*, In: Gerhard J., Kotsireas I. (eds) Maple in Mathematics Education and Research. MC 2019. Communications in Computer and Information Science, vol 1125. Springer (2020)


Here we will briefly comment on the programming details required to achieve the images from the paper, as well as other functionalities not mentioned.
The package is divided into two parts: Wythoff and Zome, which we shall document individually below.

## Wythoff

Two things are necessary for Wythoff's construction: a finite reflection group, and a point to be reflected.
This package contains the code to generate most reflection groups one cares about, as well as the points relative to them that give uniform polytopes.
A basic function is `SimpleRoots`, which contains choices of simple roots for the root systems of type _B, D, I_ as well as the exceptions _F4_ and _E8_.
The dimension of euclidean space the roots are given in is such that they are always a basis.
In future, support for other root systems will be added (e.g. _A2, A3, A4_ and _E6, E7_).

### Wythoff Constructions

The main feature of this package is to perform Wythoff constructions.
This is very easy: an example is as follows.

```
V, E := Wythoffian("B", 3, [3]);
```

Here, _V, E_ are the vertices and edges respectively of the construction.
The constructed polytope is the one indicated by the Coxeter diagram for _B3_ with its third box uncrossed (see Champagne-Kjiri-Patera-Sharp for notation).
In the future, a function will be added to draw Coxeter diagrams given these parameters, as well as compute their f-vectors from diagrams.

When working with large groups, it will be useful to provide an optional fourth argument to Wythoff, which is a preconstructed group.
We have provided `H4.mla`, which contains the only 4-dimensional Coxeter group that is large enough to prove problematic when it comes to computation.
When generating _H4_ polytopes, something like the following should be used:
```
read "H4.mla";
V, E := Wythoffian("H", 4, [1,3], H4); # The cantellated 120-cell
```

### Projecting and Creating Cells

For polytopes in three and four dimensions, the `CreateCells` function takes as input a list of edges and a list of vertices and returns a list of cells.
These cells are used in most of the rest of this package.
Before cells can be used e.g. in drawing or Zome constructions, they must be projected down into three dimensions.
This package provides a set of basic projections that are shown below:

```
V, E := Wythoffian("B", 4, [1, 2]);
C := CreateCells(E, V);
ProjectCells(C, StereographicProject([1,0,0,0]));  # Typical north-pole stereographic projection
ProjectCells(C, BasisProject([1,1,0,0], [0,1,1,0], [0,0,1,1]));  # Project onto an arbitrary basis. It will be run through Gram-Schmidt first.
ProjectCells(C, OrthonormalProject([1,0,0,0], [0,1,0,0], [0,0,1,0]));  # Project onto an orthonormal basis. It will not be run through Gram-Schmidt, so it may look weird if a non-orthonormal basis is used.
ProjectCells(C, ProjectThroughPoint([1,0,0,0]));  # Project onto the hyperplane orthogonal to the given point.
ProjectCells(C);  # This uses the default projection, wForget, which removes the last coordinate.
```

### Coxeter Plane Projections

We calculate the Coxeter plane by finding the eigenvalues of a Coxeter element as described in Humphreys's _Reflection and Coxeter Groups_, section 3.17.
The function `CoxeterPlane` takes as input the type of root system and its rank and outputs a basis for the Coxeter plane.
To project through a Coxeter plane, we can use the `OrthonormalProject` function from the Wythoff package.
Below is an example program to draw a Coxeter plane projection.

```
# Assume C is a list of cells from CreateCells, for some 4d polytope
# This performs a projection onto the H4 Coxeter plane
B := CoxeterPlane("H", 4);
ProjectCells(C, OrthonormalProject(B));
RoughDraw(C);
```

## Zome

The functionality of the Zome package is split across two modules; `Model` is a Zome model, while `System` is a particular kind of Zome system.
The typical Zome system, with angles based on icosahedral symmetry and lengths based on the golden ratio, is available in the provided file `IcoZome.mla`.

### Creating Models

To make a model, two pieces of information are needed: a list of cells, and the Zome system that the model should be constructed in.
Alternatively, a list of edges can be provided instead of a list of cells.
Running the function `Zome:-Model` with this inforrmation will attempt to construct the model in the given Zome system.
In the event that one is already sure their model is constructible, it is possible to skip this construction step by instead indicating the longest type of piece used.
If we have a list of cells `C`, and we have loaded the `IcoZome` system from `IcoZome.mla`, consider the following two pieces of code:

```
Zome:-Model(C, IcoZome);
Zome:-Model(C, IcoZome, ["B",2]);
```

The first line will check whether `C` can be made in Zome.
The second will assume it can be constructed, and will be significantly faster.

One additional feature the Model module supports is resolution of self intersections.
When working with structures such as the 24-cell, it is possible to find projections that send lines to intersecting lines in 3-space.
Despite this, the projection may still be constructible if instead a vertex is placed at the crossing point, and the two edges replaced with four.
This operation of adding a vertex and replacing edges is performed by the function `RemoveSelfIntersections` on a model.

### Creating Zome Systems

For most users, the provided `IcoZome` system will be the only one necessary.
For those interested in an alternate Zome system, for example based on octahedral symmetry, or perhaps in a Zome system that does not include green struts, the following may be informative.

Four pieces of information are needed.
First is a list of labels on the pieces: for Zome, this is `["B", "R", "G", "Y"]`.
Then a table of labels to colours; this will control the colours in which Zome models are displayed.
Third is a table of cosines; a key might be `["B", "G"]`, corresponding to a list of all possible cosines between edges of colour blue and green.
Finally, a table of label to ratios to some distinguished piece; in the provided `IcoZome`, a piece `B2` is considered to have length 1, and this table gives all other lengths relative to this one.

With this information, a new system can be constructed using `Zome:-System(...)`.

### Displaying Zome Models

Every Zome system exports functions `Draw` and `Display`.
The `Display` function takes as input a model, and outputs a plot.
The `Draw` function instead outputs the list of things to be displayed.
This would be used, for example, if you wanted to merge multiple drawings into one plot.

```
# This prints our model to the screen
MyZomeSystem:-Display(Model1);

# Zome:-Draw does not, so we can use it to put two models together
plots:-display([op(MyZomeSystem:-Draw(Model1)), op(MyZomeSystem:-Draw(Model2))])
```

### Partial Drawings

In the paper, we demonstrated using the package to construct "recipes" for Zome models by drawing them bit by bit.
This functionality is provided by the partial drawing functions, `LocalDraw` and `PartialDraw`.
Currently, these functions work with cells, although in future the option will be added to build by edges also.

The first of the two functions is used to draw the model about a given point.
It takes as input a model, a distinguished cell, a distance, and a distance function in that order.
The distinguished cell is printed in its Zome colours, and all other cells within the given distance (according to the distance function) are printed in black.

```
# Borrow PointNorm from Wythoff, for the usual Euclidean norm.
LocalDraw(MyModel, MyModel:-Cells[1], 0.5, PointNorm);
```

The latter, `PartialDraw` is slightly more complicated.
It takes as input a model, a height function, a distinguished cell, a boolean flag, and a filter function.
The final two arguments are optional.
It prints all cells with height strictly less than that of the distinguished cell in black, and the distinguished cell, as well as all vertices at the same height as it, in Zome colours.
If the optional flag (default true) is set to false, instead it only prints the distinguished cell in Zome colour, and does not print other cells at the same height.
If a filter function is passed, only cells whose center of mass meets the filter condition are displayed.

```
# This shows how to attach a single cell when the model is being built from the ground up.
PartialDraw(MyModel, x -> x:-CenterOfMass[3], MyModel:-Cells[1], false);

# This shows how to attach the layer of cells containing MyModel:-Cells[1], when the model is being built radially outwards.
PartialDraw(MyModel, x -> PointNorm(x:-CenterOfMass), MyModel:-Cells[1]);
```
