Wythoff := module()
	option package;
	local cleanNumber, cleanVector, DistSq, PtsToEdges, PtsToEdgesWithGoldenRatio, evalGoldenRatio, evalfGoldenRatio,
		  AreColinear, PolygonVerticesAndEdges, StarFormula, StarProduct, Dot, SimplexNormal, SimplexNormal3d,
		  NormalsToSimplices, NormalsToSimplices3d, GroupCellVertices, CellEdgesInVerticesLabels, CellsEdgesInVerticesLabels,
		  createCell, createFace, CreateCell, CreateFace, CreateFctsAvailable, TypeCellsAvailable,
		  CoxeterMatrix, OperatorToMatrix, ReflectionThroughVector, ComplexAngleF, 
		  RemovePointProjections, VerticesFromTypicalPoint, PointNorm, PointNormSquared,
	      UniformEdgesFromVertices, StdBasis, SimplexVertices, CompleteVectorsToBasis,
		  TypicalPoint, MatrixFromListOfVectors, CompleteBasis, ONBasisFromOneVector, moins,
		  FindNormalVector, FindEquator, IsInUpperHemisphere, PickUpperHemisphere, UniquePoints, UniqueEdges;

	export Wythoffian, CreateCells, SimpleRoots, wForget, ProjectCells, BreakByLevels, BasisProject, OrthonormalProject, GenerateGroupFromSimpleRoots,
		   CoxeterPlane, OrthonormalProjectF, ProjectThroughPoint, Midpoint, StereographicProject, Alternate, RoughDraw, RoughDisplay, CreateRotation;

	cleanNumber := proc (x)
	    if x = 0 or degree(x, tau) = 0 or degree(x, tau) = 1 then
	        x
	    elif degree(x, tau) < 0 then
	        cleanNumber(expand(subs({1/tau^2 = 2-tau, 1/tau = tau-1}, expand(x))))
	    else
	        cleanNumber(subs(tau^degree(x, tau) = tau^(degree(x, tau)-2)*(tau+1), expand(x)))
	    end if
	end proc;

	cleanVector := p -> map(cleanNumber, p);

	DistSq := (A, B) -> add((A[i]-B[i])^2, i = 1 .. nops(A));

	PtsToEdges := proc (pts, LSq := 4)
	    local AllEdges;
	    AllEdges := {seq(seq({pts[i], pts[j]}, i = 1 .. j-1), j = 2 .. nops(pts))};
	    map(e -> if simplify(DistSq(op(e))-LSq) = 0 then e else NULL end if, AllEdges)
	end proc;

	PtsToEdgesWithGoldenRatio := proc (pts, LSq := 4)
	    local CleanPoints, AllEdges;
	    CleanPoints := map(cleanVector, subs(sqrt(5) = 2*tau-1, pts));
	    AllEdges := {seq(seq({CleanPoints[i], CleanPoints[j]}, i = 1 .. j-1), j = 2 .. nops(CleanPoints))};
	    map(e -> if subs(tau^2 = tau+1, expand(DistSq(op(e))-LSq)) = 0 then e else NULL end if, AllEdges)
	end proc;

	evalGoldenRatio := x -> subs(tau = 1/2+(1/2)*sqrt(5), x);
	evalfGoldenRatio := x -> evalf(subs(tau = 1/2+(1/2)*sqrt(5), x));

	AreColinear := proc (p, q, r)
	    evalb(not(solve(expand(t*p+(1-t)*q-r), t) = NULL));
	end proc;

	PolygonVerticesAndEdges := proc (Pts)
	    local PtsList, CenterOfMass, v, w, ii, N, n, plist, ttt;
	    N := nops(Pts);
	    PtsList := convert(Pts, list);
	    CenterOfMass := add(Pts)/N;
	    for ii to 3 do
	        v[ii] := Pts[ii]-CenterOfMass
	    end do;
	    if Rank(Matrix(2, 3, [op(v[1]), op(v[2])])) = 2 then
	        w := v[2]
	    else
	        w := v[3]
	    end if;

	    ttt := map(p -> simplify([Dot(p, v[1]), Dot(p, w)]), PtsList);
	    plist := ComputationalGeometry:-ConvexHull(ttt);
	    n := nops(plist);
	    PtsList := map(i -> PtsList[i], plist);
	    N := i -> if i = 0 then n elif i = n+1 then 1 else i end if;
	    map(i -> if AreColinear(PtsList[N(i-1)], PtsList[N(i)], PtsList[N(i+1)]) then NULL else i end if, [seq(i, i = 1 .. n)]);
	    PtsList := map(i -> PtsList[i], %);
	    n := nops(PtsList);
	    PtsList, [seq({PtsList[i], PtsList[i+1]}, i = 1 .. n-1), {PtsList[1], PtsList[n]}]
	end proc;

	LinearAlgebra:-Determinant(Matrix(4, 4, [e[1], e[2], e[3], e[4], seq(t[i], i = 1 .. 12)]));
	StarFormula := [seq(coeff(%, e[i]), i = 1 .. 4)];
	StarProduct := proc (v1, v2, v3)
	    [op(v1), op(v2), op(v3)];
	    subs([seq(t[i] = %[i], i = 1 .. 12)], StarFormula)
	end proc;

	#
	Dot := (v, w) -> add(v[i]*w[i], i = 1 .. nops(v));

	SimplexNormal := proc (p1, p2, p3, p4, evalProc := evalf)
	    local v, a;
	    v := simplify(StarProduct(p2-p1, p3-p1, p4-p1));
	    if v = [0, 0, 0, 0] then
	        v
	    else
	        a := sign(evalProc(Dot(v, p1)))/sqrt(Dot(v, v));
	        evalf(map(x -> simplify(x*a), v));
	        evalf(%, 3)
	    end if
	end proc;

	SimplexNormal3d := proc (p1, p2, p3, evalProc := evalf)
	    local v, a; v := convert(simplify(LinearAlgebra:-CrossProduct(p2-p1, p3-p1)), list);
	    if v = [0, 0, 0] then
	        v
	    else
	        a := sign(evalProc(Dot(v, p1)))/sqrt(Dot(v, v));
	        evalf(map(x -> simplify(x*a), v));
	        evalf(%, 3)
	    end if
	end proc;

	NormalsToSimplices := proc (Simplices, Vertices)
	    map(simplex -> SimplexNormal(op(Vertices[simplex])), Simplices)
	 end proc;

	NormalsToSimplices3d := proc (Simplices, Vertices)
	    map(simplex -> SimplexNormal3d(op(Vertices[simplex])), Simplices)
	end proc;

	GroupCellVertices := proc (Simplices, Vertices)
	    local GroupingSet, Normals, dim;
	    dim := nops(Vertices[1]);
	    if dim = 3 then
	        Normals := NormalsToSimplices3d(Simplices, Vertices)
	    elif dim = 4 then
	        Normals := NormalsToSimplices(Simplices, Vertices)
	    else
	        print(cat("procedure GroupCellVertices not programmed for dimension ", dim));
	        EXIT
	    end if;
	    GroupingSet := {op(Normals)} minus {[0, 0, 0, 0]};
	    {seq({ListTools:-SearchAll(v, Normals)}, v in GroupingSet)};
	     map(l -> map(i -> Simplices[i], l), %);
	     map(l -> `union`(op(map(convert, l, set))), %)
	end proc;

	CellEdgesInVerticesLabels := proc (celluleInVerticesLabels, edgesInVerticesLabels)
	    local completeGraphEdges;
	    completeGraphEdges := combinat:-choose(celluleInVerticesLabels, 2);
	    select(e -> evalb(e in edgesInVerticesLabels), completeGraphEdges);
	    return %
	end proc;

	CellsEdgesInVerticesLabels := proc (cellsInVerticesLabels, edgesInVerticesLabels)
	    map(cellule -> CellEdgesInVerticesLabels(cellule, edgesInVerticesLabels), cellsInVerticesLabels)
	end proc;

	createCell := proc (vertices, edges)
	    module ()
	    export V, E, F, Vertices, Edges, CenterOfMass, NormalVector, Edges3D, CenterOfMass3D, Level, label, ModulePrint;
	    V := nops(vertices);
	    E := nops(edges);
	    F := 2-V+E;
	    Vertices := vertices;
	    Edges := edges;
	    CenterOfMass := add(vertices)/V;
	    Level := NULL;
	    label := NULL;
	    ModulePrint := proc ()
	        if Level = NULL then
	            if label = NULL then
	                `C`[F]
	            else
	                label
	            end if
	        elif label = NULL then
	            `C`[F, Level]
	        else
	            label[Level]
	        end if
	    end proc

	    end module
	end proc;

	createFace :=
	    proc (vertices, edges)
	    module ()
	    export V, E, Vertices, Edges, CenterOfMass, NormalVector, Edges3D, CenterOfMass3D, Level, label, ModulePrint;
	    V := nops(vertices);
	    E := nops(edges);
	    Vertices := vertices;
	    Edges := edges;
	    CenterOfMass := add(Vertices)/V;
	    label := NULL;
	    ModulePrint := proc ()
	        if label = NULL then
	            {V}
	        else
	            label
	        end if
	    end proc

	    end module
	end proc;

	CreateCell := proc (vertices, cellEdgesInVerticesLabels, cellInVerticesLabels)
	    createCell(map(v -> vertices[v], cellInVerticesLabels), map(e -> {vertices[e[1]], vertices[e[2]]}, cellEdgesInVerticesLabels))
	end proc;

	CreateFace := proc (vertices, cellEdgesInVerticesLabels, cellInVerticesLabels)
	    createFace(map(v -> vertices[v], cellInVerticesLabels), map(e -> {vertices[e[1]], vertices[e[2]]}, cellEdgesInVerticesLabels))
	end proc;

	CreateFctsAvailable := table([3 = createFace, 4 = CreateCell]);
	TypeCellsAvailable := table([3 = "face", 4 = "cell"]);

	CreateCells := proc (Edges, Vertices)
	    local dim, CreateFct, TypeCellCreated, simplicesInVerticesLabels, cellsInVerticesLabels, edgesInVerticesLabels, cellsEdgesInVerticesLabels;
	    dim := nops(Vertices[1]);
	    if dim in {3, 4} then
	        CreateFct := CreateFctsAvailable[dim];
	        TypeCellCreated := TypeCellsAvailable[dim]
	    else
	        print(cat("procedure CreateCells not programmed for dimension ", dim));
	        EXIT
	    end if;
	    if dim = 3 then
	        print("Do not trust this result too much. Some of the faces are potentially inside the convex hull.");
	        print("")
	    end if;
	    print(cat("dimension is ", dim, ", ", TypeCellCreated, "s created"));
	    simplicesInVerticesLabels := ComputationalGeometry:-ConvexHull(Vertices);
	    print("1.Simplices computed");
	    GroupCellVertices(simplicesInVerticesLabels, Vertices);
	    cellsInVerticesLabels := convert(%, list);
	    print(cat("2.", TypeCellCreated, "s computed in terms of Vertices labels"));
	    if dim = 3 then
	        map(l -> map(i -> Vertices[i], l), cellsInVerticesLabels);
	        map(x -> CreateFct(PolygonVerticesAndEdges(x)), %)
	    else
	        edgesInVerticesLabels := map(e -> map(ListTools:-Search, e, Vertices), Edges);
	        print("3.Edges computed in terms of Vertices labels");
	        cellsEdgesInVerticesLabels := CellsEdgesInVerticesLabels(cellsInVerticesLabels, edgesInVerticesLabels);
	        print(cat("4.", TypeCellCreated, " edges computed in terms of Vertices labels"));
	        [seq(CreateFct(Vertices, cellsEdgesInVerticesLabels[i], cellsInVerticesLabels[i]), i = 1 .. nops(cellsEdgesInVerticesLabels))]
	    end if
	end proc;

	# SimpleRoots takes in the type of Coxeter group and size parameter n, and returns an appropriate set of simple roots for that root system.
	SimpleRoots := proc (t, n) local M, i;
	    if t = "A" then
	        M := Matrix(n, n, storage = sparse);
	        M[1,1] := 1;
	        for i to n-1 do
	            M[i, i+1] := 1/(-2*M[i,i]);
	            M[i+1, i+1] := sqrt(1-M[i,i+1]^2);
	        end do;
	        return [seq(convert(LinearAlgebra:-Column(M, i), list), i = 1 .. n)]
	    elif t = "B" or t = "C" then
	        M := Matrix(n, storage = sparse);
	        for i to n-1 do
	            M[i, i] := 1;
	            M[i, i+1] := -1
	        end do;
	        M[n, n] := 1;
	        if t = "C" then
	            M[n, n] := 2
	        end if;
	        return [seq(convert(LinearAlgebra:-Row(M, i), list), i = 1 .. n)]
	    elif t = "D" then
	        M := Matrix(n, storage = sparse);
	        for i to n-1 do
	            M[i, i] := 1;
	            M[i, i+1] := -1
	        end do;
	        M[n, n] := 1;
	        M[n, n-1] := 1;
	        return [seq(convert(LinearAlgebra:-Row(M, i), list), i = 1 .. n)]
	    elif t = "E" then
	        if n = 6 then
	        elif n = 7 then
	        elif n = 8 then
	            return [[1, -1, 0, 0, 0, 0, 0, 0], [0, 1, -1, 0, 0, 0, 0, 0], [0, 0, -1, 1, 0, 0, 0, 0], [0, 0, 0, -1, 1, 0, 0, 0], [0, 0, 0, 0, -1, 1, 0, 0], [0, 0, 0, 0, 0, -1, 1, 0], [0, 0, 0, 0, 0, 1, 1, 0], [-1/2, -1/2, -1/2, -1/2, -1/2, -1/2, -1/2, -1/2]]
	        end if
	    elif t = "F" then
	        if n = 4 then
	            return [[0, 1, -1, 0], [0, 0, 1, -1], [0, 0, 0, 1], [1/2, -1/2, -1/2, -1/2]]
	        end if
	    elif t = "G" then
	        if n = 2 then
	            return SimpleRoots("I", 6)
	        end if
	    elif t = "H" then
	        if n = 3 then
	            return [[1/4+(1/4)*sqrt(5), -1/2, -1/4+(1/4)*sqrt(5)], [-1/4-(1/4)*sqrt(5), 1/2, -1/4+(1/4)*sqrt(5)], [1/2, -1/4+(1/4)*sqrt(5), -1/4-(1/4)*sqrt(5)]]
	        elif n = 4 then
	            return [[1/4+(1/4)*sqrt(5), -1/2, -1/4+(1/4)*sqrt(5), 0], [-1/4-(1/4)*sqrt(5), 1/2, -1/4+(1/4)*sqrt(5), 0], [1/2, -1/4+(1/4)*sqrt(5), -1/4-(1/4)*sqrt(5), 0], [-1/2, -1/4-(1/4)*sqrt(5), 0, -1/4+(1/4)*sqrt(5)]]
	        end if
	    elif t = "I" then
	        return [[1, 0], [cos(2*Pi/n), sin(2*Pi/n)]]
	    end if;
	    print("Unrecognized root system ", t, n)
	end proc;

	# Given a set of simple roots, we want to turn it into the matrix associated to the Coxeter diagram.
	CoxeterMatrix := proc (simpleSystem)
	    [seq([seq(-cos(Pi/denom(simplify(LinearAlgebra:-VectorAngle(convert(simpleSystem[i], Vector), convert(simpleSystem[j], Vector))))), j = 1 .. nops(simpleSystem))], i = 1 .. nops(simpleSystem))]
	end proc;

	# Given a linear operator on Rn, we can put it into matrix form
	OperatorToMatrix := proc(T, dim := 4)
	    local i, id, rows:
	    rows := []:
	    id := LinearAlgebra:-IdentityMatrix(dim):
	    for i from 1 to dim do
	        rows := [op(rows), T(LinearAlgebra:-Transpose(LinearAlgebra:-Row(id, i)))]:
	    od:
	    LinearAlgebra:-Transpose(convert(rows, Matrix))
	end:

	# Given a vector, we can construct the operator that acts by reflection through the plane orthogonal to that vector:
	ReflectionThroughVector := proc (P)
	    proc (v)
	        v-(2*v . P)*P/(P . P)
	    end proc
	end proc;

	ComplexAngleF := proc (w)
	    local x, y;
	    x := evalf(Re(w));
	    y := evalf(Im(w));
	    if x < 0 and y = 0 then
	        Pi
	    else
	        2*arctan(y/(sqrt(x^2+y^2)+x))
	    end if
	end proc;

	# Finally we can compute the Coxeter plane of a Coxeter group. This plane is invariant under any Coxeter element.
	CoxeterPlane := proc (t, n)
		local simpleSystem, M, V;
		simpleSystem := SimpleRoots(t, n);
	    M := foldl((x, y) -> x . y, LinearAlgebra:-IdentityMatrix(n, n), op(map(x -> OperatorToMatrix(ReflectionThroughVector(convert(x, Vector)), n), simpleSystem)));
	    LinearAlgebra:-Eigenvectors(M, output = list);
	    select(x -> simplify(abs(x[1])-1) = 0, %);
	    map(x -> [ComplexAngleF(x[1]), x[3][1]], %);
	    select(x -> is(0 < x[1]), %);
	    sort(%, (x, y) -> is(x[1] < y[1]));
	    V := %[1][2];
	    [map(Re, V), map(Im, V)]
	end proc;

	wForget := proc (PointOrEdge)
	    if type(PointOrEdge, set) then
	        map(thisproc, PointOrEdge)
	    else
	        [seq(PointOrEdge[i], i = 1 .. 3)]
	    end if
	end proc;

	RemovePointProjections := SetOrList -> map(x -> if nops(x) = 1 then NULL else x end if, SetOrList);

	ProjectCells := proc (CellList, ProjectionFct := wForget)
	    local c;
	    for c in CellList do
	        c:-Edges3D := RemovePointProjections(map(ProjectionFct, c:-Edges));
	        c:-CenterOfMass3D := ProjectionFct(c:-CenterOfMass);
	        c:-Level := expand(Dot(c:-CenterOfMass3D, c:-CenterOfMass3D)):
	    end do:
	end proc;

	BreakByLevels := proc (CellList)
	    local levels;
	    levels := sort([op({op(map(c -> c:-Level, CellList))})], (a, b) -> evalb(evalf(a) < evalf(b)));
	    [levels, map(level -> select(c -> c:-Level = level, CellList), levels)]
	end proc;

	BasisProject := proc (B) local ONB;
	    ONB := LinearAlgebra:-GramSchmidt(B, normalized);
	    proc (PointOrEdge)
	        local v, u, i;
	        if type(PointOrEdge, set) then
	            map(thisproc, PointOrEdge)
	        else
	            v := convert(PointOrEdge, Vector);
	            u := add(v . ONB[i] . ONB[i], i = 1 .. nops(ONB));
	            convert(LinearAlgebra:-LinearSolve(convert(B, Matrix), convert(u, Vector)), list)
	        end if
	    end proc
	end proc;

	OrthonormalProject := proc (B)
	    proc (PointOrEdge)
	        local v, u, i;
	        if type(PointOrEdge, set) then
	            map(thisproc, PointOrEdge)
	        else
	            v := convert(PointOrEdge, Vector);
	            u := add((v . B[i])*B[i], i = 1 .. nops(B));
	            map(x -> u . x, B)
	        end if
	    end proc
	end proc;

	OrthonormalProjectF := proc (B)
	    local B2, K;
	    K := map(x -> map(Re, evalf(expand(x))), B);
	    B2 := LinearAlgebra:-GramSchmidt(map(x -> convert(x, Vector), K), normalized);
	    proc (PointOrEdge)
	        local v, u, i;
	        if type(PointOrEdge, set) then
	            map(thisproc, PointOrEdge)
	        else
	            v := evalf(convert(PointOrEdge, Vector));
	            u := add((v . B2[i])*B2[i], i = 1 .. nops(B2));
	            map(x -> u . x, B2)
	        end if
	    end proc
	end proc;

	ProjectThroughPoint := proc (v)
	    OrthonormalProject(map(x -> convert(x, Vector), ONBasisFromOneVector(v)[2 .. -1]))
	end proc;

	Midpoint := proc (Edge)
	    (1/2)*Edge[1]+(1/2)*Edge[2]
	end proc;

	StereographicProject := proc (point)
	    local M, P;
	    M := CreateRotation(point);
	    proc (PointOrEdge)
	        if type(PointOrEdge, set) then
	            map(thisproc, PointOrEdge)
	        else
	            P := M . convert(PointOrEdge, Vector);
	            if PointOrEdge[-1] = 1 then
	                [seq(0, i = 1 .. nops(PointOrEdge)-1)]
	            else
	                [seq(P[i]/(1-P[-1]), i = 1 .. nops(PointOrEdge)-1)]
	            end if
	        end if
	    end proc
	end proc;

	# Wythoff Constructions
	# Treat a finite reflection group W as a subset of GL(R^n), and construct the Cayley graph on this group generated by reflections through a simple root system.
	# The graph can be enumerated via a breadth first search. The output is a set of matrices: the elements of W realized as linear operators on R^n
	#
	GenerateGroupFromSimpleRoots := proc (simpleSystem)
	    local S, q, seen, x, M, dim, vxs, unseen, i, k;
	    option remember;
	    dim := nops(simpleSystem[1]);
	    S := map(x -> OperatorToMatrix(ReflectionThroughVector(convert(x, Vector)), dim), simpleSystem);
	    q := queue[new](LinearAlgebra:-IdentityMatrix(dim));
	    seen[0] := [LinearAlgebra:-IdentityMatrix(dim), 0];
	    vxs := {LinearAlgebra:-IdentityMatrix(dim)};
	    k := 1;
	    while not queue[empty](q) do
	        x := queue[dequeue](q);
	        for M in map(Y -> simplify(expand(x . Y)), S) do
	            unseen := true;
	            for i in map(op, [indices(seen)]) do
	                if IsMatrixShape(simplify(seen[i][1]-M), zero) then
	                    if nops(S) <= seen[i][2]+1 then
	                        seen[i] := evaln(seen[i])
	                    else
	                        seen[i] := [seen[i][1], seen[i][2]+1]
	                    end if;
	                    unseen := false
	                end if
	            end do;
	            if unseen then seen[k] := [M, 1];
	                k := k+1;
	                vxs := {op(vxs), M};
	                queue[enqueue](q, M)
	            end if
	        end do
	    end do;
	    vxs
	end proc;

	# Given a group representation like this, we can easily compute the orbit of a point, and from this turn it into its convex hull.
	# Under the assumption of uniformity (which is typcial of our Wythoff constructions) we can turn vertices into edges given the edge length.
	# We would like to pick this as the smallest length, however this may fail--e.g. for star polyhedra.
	# Instead, look at the nonzero distances under reflection of our typical point.
	VerticesFromTypicalPoint := proc (G, x)
	    map(y -> y . convert(x, Vector), G);
	    [op(map(x -> convert(x, list), %))]
	end proc;

	PointNormSquared := proc (v)
	    add(v[i]^2, i = 1 .. nops(v))
	end proc;

	PointNorm := proc (v)
	    sqrt(PointNormSquared(v))
	end proc;

	UniformEdgesFromVertices := proc (Vertices, delta, epsilon := 0.1e-3)
	    local UniformEdgesFromVerticesHelper;
	    UniformEdgesFromVerticesHelper := proc (vxs, delta)
	        local left, right, middle;
	        if nops(vxs) <= 1 then
	            return {}
	        end if;
	        sort(vxs, (x, y) -> is(evalf(x[1][1]-y[1][1]) <= epsilon));
	        left := %[1 .. floor((1/2)*nops(vxs))];
	        right := `%%`[floor((1/2)*nops(vxs))+1 .. -1];
	        middle := select(x -> is(evalf(abs(x[1][1]-right[1][1][1])) <= delta+epsilon), vxs);
	        if evalf(abs(left[1][1][1]-right[1][1][1])) <= delta+epsilon then
	            left := {}
	        else
	            left := thisproc(left, delta)
	        end if;
	        if evalf(abs(right[-1][1][1]-right[1][1][1])) <= delta+epsilon then
	            right := {}
	        else
	            right := thisproc(right, delta)
	        end if;
	        if nops(middle[1][1]) = 1 then
	            middle := select(x -> is(evalf(PointNorm(x[1]-x[2])) <= delta+epsilon), {seq(seq([middle[i][2], middle[j][2]], j = i+1 .. nops(middle)), i = 1 .. nops(middle))})
	        else
	            middle := thisproc(map(x -> [x[1][2 .. -1], x[2]], middle), delta)
	        end if;
	        return {op(left), op(right), op(middle)}
	    end proc;
	    UniformEdgesFromVerticesHelper(map(x -> [x, x], Vertices), delta)
	end proc;

	#
	# To pick typcial points for Wythoff's construction, we need a few linear algebra tools.
	# First will be to pick out vertices from a simplex, and second to find bisectors of hyperplanes.
	# For this function, a simplex will be described by n linearly independent normal vectors.
	StdBasis := proc (n, i)
	    convert([seq(`if`(j = i, 1, 0), j = 1 .. n)], Vector)
	end proc;

	SimplexVertices := proc (normals)
	    local M;
	    M := Matrix(normals);
	    [seq(LinearAlgebra:-Normalize(LinearAlgebra:-LinearSolve(M, StdBasis(nops(normals), i)), 2), i = 1 .. nops(normals))]
	end proc;

	CompleteVectorsToBasis := proc (vs) local B, B2;
	    if nops(vs) = 1 then
	        B := [vs[1], seq(convert(StdBasis(nops(vs[1]), i), list), i = 2 .. nops(vs[1]))];
	        if LinearAlgebra:-Determinant(Matrix(B)) = 0 then
	            B[2] := convert(StdBasis(nops(vs[1]), 1), list)
	        end if;
	        return B
	    end if;
	    B := CompleteVectorsToBasis(vs[1 .. -2]);
	    B2 := B;
	    B2[nops(vs)] := vs[-1];
	    if LinearAlgebra:-Determinant(Matrix(B2)) = 0 then
	        B[nops(vs)+1] := B[nops(vs)];
	        B[nops(vs)] := vs[-1] else B[nops(vs)] := vs[-1]
	    end if;
	    return B
	end proc;

	TypicalPoint := proc (simpleSystem, ringed)
	    local V, i, j, normals, u, M, B;
	    V := SimplexVertices(simpleSystem);
	    if nops(ringed) = 1 then
	        return V[ringed[1]]
	    end if;
	    normals := {};
	    for i to nops(ringed) do
	        for j from i+1 to nops(ringed) do
	        	print(simpleSystem[ringed[i]]/PointNorm(simpleSystem[ringed[i]]));
	        	print(simpleSystem[ringed[j]]/PointNorm(simpleSystem[ringed[j]]));
	            normals := {op(normals), simplify(expand(simpleSystem[ringed[i]]/PointNorm(simpleSystem[ringed[i]])-simpleSystem[ringed[j]]/PointNorm(simpleSystem[ringed[j]])))}
	        end do
	    end do;
	    print(normals);
	    [seq(V[ringed[i]]-V[ringed[1]], i = 2 .. nops(ringed))];
	    map(x -> convert(x, Vector), CompleteVectorsToBasis(map(x -> convert(x, list), %)));
	    LinearAlgebra:-GramSchmidt(%);
	    u := map(x -> convert(x, list), %[nops(ringed) .. -1]);
	    M := Matrix([seq([op(normals[i]), op(convert(LinearAlgebra:-ZeroVector(nops(simpleSystem)), list))], i = 1 .. nops(normals)), seq([op(convert(LinearAlgebra:-ZeroVector(nops(simpleSystem)), list)), op(u[i])], i = 1 .. nops(u)), seq([op(convert(StdBasis(nops(simpleSystem), i), list)), op(convert(-StdBasis(nops(simpleSystem), i), list))], i = 1 .. nops(simpleSystem))]);
	    B := convert([op(convert(LinearAlgebra:-ZeroVector(nops(normals)+nops(u)), list)), op(convert(V[ringed[1]], list))], Vector);
	    LinearAlgebra:-Normalize(LinearAlgebra:-LinearSolve(M, B)[1 .. nops(simpleSystem)], 2)
	end proc;

	UniquePoints := proc (Pts, epsilon := 0.1e-3)
	    local distances, strata, stratum, i;
	    if Pts = {} then
	        return {}
	    end if;
	    distances := sort([seq([evalf(PointNormSquared(Pts[i]-Pts[1])), Pts[i]], i = 1 .. nops(Pts))], (x, y) -> is(x[1] < y[1]));
	    strata := [];
	    stratum := {};
	    for i from 2 to nops(distances) do
	        if epsilon < distances[i][1]-distances[i-1][1] then
	            strata := [op(strata), stratum];
	            stratum := {distances[i][2]} else stratum := {distances[i][2], op(stratum)}
	        end if
	    end do;
	    strata := [op(strata), stratum];
	    if nops(strata) = 1 then
	        return {Pts[1]}
	    else
	        return {Pts[1]} union foldl(`union`, {}, op(map(UniquePoints, strata[2 .. -1], epsilon)))
	    end if
	end proc;

	UniqueEdges := proc (Edges, epsilon := 0.1e-3)
	    local UniqueEdgesHelper;
	    UniqueEdgesHelper := proc (Pts, epsilon := 0.1e-3)
	        local distances, strata, stratum, i;
	        if Pts = {} then
	            return {}
	        end if;
	        distances := sort([seq([evalf(PointNormSquared(Pts[i][1]-Pts[1][1])), Pts[i]], i = 1 .. nops(Pts))], (x, y) -> is(x[1] < y[1]));
	        strata := [];
	        stratum := {};
	        for i from 2 to nops(distances) do
	            if epsilon < distances[i][1]-distances[i-1][1] then
	                strata := [op(strata), stratum];
	                stratum := {distances[i][2]} else stratum := {distances[i][2], op(stratum)}
	            end if
	        end do;
	        strata := [op(strata), stratum];
	        if nops(strata) = 1 then
	            return {Pts[1][2]} else return {Pts[1][2]} union foldl(`union`, {}, op(map(UniqueEdgesHelper, strata[2 .. -1], epsilon)))
	        end if
	    end proc;
	    UniqueEdgesHelper(map(x -> [Midpoint(x), x], Edges), epsilon)
	end proc;

	Wythoffian := proc (t, n, r, g := [])
        local simpleSystem, G, Pt, Vertices, Edges, delta;
	    simpleSystem := SimpleRoots(t, n);
	    if nops(g) = 0 then
            G := GenerateGroupFromSimpleRoots(simpleSystem);
        else
            G := g;
        end if;
	    Pt := TypicalPoint(simpleSystem, r);
	    map(y -> PointNormSquared(convert((ReflectionThroughVector(convert(y, Vector)))(Pt), list)-convert(Pt, list)), simpleSystem);
	    select(x -> is(0 < x), %);
	    delta := evalf(max(%));
	    Vertices := map(x -> simplify(expand(x)), [op(UniquePoints(VerticesFromTypicalPoint(G, Pt)))]);
	    Edges := map(x -> simplify(expand({op(x)})), UniqueEdges(UniformEdgesFromVertices(Vertices, evalf(sqrt(delta)))));
	    return Vertices, Edges
	end proc;

	# We would like to be able to construct snubs. One way to envision a snub is as the alternation of an omnitruncate, so we should be able to manage via a Wythoff construction
	Alternate := proc (Vertices, Edges)
	    local colour, z, DFS, delta, Vert, Edge;
	    colour := table([seq(v = "GREY", v in Vertices)]);
	    z := [seq(0, i = 1 .. nops(Vertices[1]))];
	    DFS := proc (v)
	        local c, e;
	        if colour[v] = "BLACK" then
	            c := "WHITE"
	        else
	            c := "BLACK"
	        end if;
	        for e in Edges do
	            if simplify(v-e[1]) = z then
	                if colour[e[2]] = "GREY" then
	                    colour[e[2]] := c;
	                    DFS(e[2])
	                end if
	            elif simplify(v-e[2]) = z then
	                if colour[e[1]] = "GREY" then
	                    colour[e[1]] := c;
	                    DFS(e[1])
	                end if
	            end if
	        end do
	    end proc;

	    colour[Vertices[1]] := "WHITE";
	    DFS(Vertices[1]);
	    Vert := select(x -> colour[x] = "WHITE", Vertices);
	    delta := min(map(x -> PointNormSquared(x-Vertices[1]), Vertices[2 .. -1]));
	    Edge := map(x -> simplify(expand({op(x)})), UniqueEdges(UniformEdgesFromVertices(Vert, evalf(sqrt(delta)))));
	    return Vert, Edge
	end proc;

	MatrixFromListOfVectors := List -> LinearAlgebra:-Transpose(Matrix(nops(List), nops(List[1]), [seq(op(List[i]), i = 1 .. nops(List))]));

	CompleteBasis := proc (v)
	    local dim, V, M, ii, PositionVector;
	    V[1] := convert(v, list);
	    dim := nops(V[1]);
	    PositionVector := [seq(i, i = 1 .. dim)];
	    for ii from 2 to dim do
	        V[ii] := map(x -> if x = ii then 1 else 0 end if, PositionVector)
	    end do;
	    M := MatrixFromListOfVectors([seq(V[i], i = 1 .. dim)]);
	    if LinearAlgebra:-Determinant(M) = 0 then
	        for ii from 2 to dim do
	            V[ii] := map(x -> x+1, V[ii])
	        end do;
	        M := MatrixFromListOfVectors([seq(V[i], i = 1 .. dim)])
	    end if;
	    M := [seq(LinearAlgebra:-Column(M, i), i = 1 .. dim)];
	    LinearAlgebra:-GramSchmidt(M);
	    map(convert, %, list)
	end proc;

	ONBasisFromOneVector := proc (v)
	    CompleteBasis(v);
	    map(x -> map(y -> y/sqrt(Dot(x, x)), x), %)
	end proc;

	CreateRotation := proc (v, w := NORTH)
	    local dim, V, W;
	    V := convert(v, list);
	    dim := nops(V);
	    if w = NORTH then
	        W := [seq(0, i = 1 .. dim-1), 1]
	    else
	        W := convert(w, list)
	    end if;
	    MatrixFromListOfVectors(ONBasisFromOneVector(W));
	    LinearAlgebra:-MatrixInverse(MatrixFromListOfVectors(ONBasisFromOneVector(V)))
	end proc;

	moins := (list1, list2) -> if nops(list1) = nops(list2) then [seq(list2[i]-list1[i], i = 1 .. nops(list1))] else print("erreur") end if;

	FindNormalVector := proc (c)
	    local n, N;
	    n := nops(c:-Vertices[1]);
	    N := c:-V-1;
	    Matrix(N, n, [seq(op(moins(c:-Vertices[i], c:-Vertices[1])), i = 2 .. N+1)]);
	    convert(op(LinearAlgebra:-NullSpace(%)), list);
	    c:-NormalVector := %;
	    RETURN*null
	end proc;

	FindEquator := CList -> select(c -> evalb(c:-NormalVector[4] = 0), CList);
	IsInUpperHemisphere := c -> evalf(`and`(op(map(e -> evalb(0 <= e[1][3]) and evalb(0 <= e[2][3]), c:-Edges3D))));
	PickUpperHemisphere := ListOfCells -> select(IsInUpperHemisphere, ListOfCells);

	RoughDraw := proc (cellorList, couleur := "black")
	    local ToDraw;
	    if type(cellorList, list) then
	        {op(map(c -> if type(c, '`module`') then op(c:-Edges3D) else c end if, cellorList))};
	        [op(%)];
	        ToDraw := map(x -> plottools:-line(op(x), color = couleur), %)
	    else
	        ToDraw := map(x -> plottools:-line(op(x), color = couleur), cellorList:-Edges3D)
	    end if;
	    ToDraw
	end proc;

	RoughDisplay := proc (cellorList, couleur := "black")
	    plots:-display(RoughDraw(cellorList, couleur), axes = none, scaling = constrained)
	end proc;
end module;
