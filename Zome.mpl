Zome := module ()
    option package;
    local AlwaysTrue, UniqueEdges, PointNorm;
    export Model, System, PartialDraw, LocalDraw, PartsList;

    PointNorm := p -> sqrt(add(p[i]^2, i=1..nops(p)));

    Model := proc(CellsOrEdges, system, longest := [])
    	module()

    	local EdgesFrom, Intersection, PointInLine, FindIntersections, SplitIntersections, UniqueEdges;
    	export Ratio, Cells, Edges, System, Longest, Vertices, RemoveSelfIntersections;

        # Given a cell or list of cells, returns its projected edges. Otherwise, acts as the identity.
    	EdgesFrom := proc(ListofCells)
            if type(ListofCells, '`module`') then
                thisproc([ListofCells])
            elif type(ListofCells[1], '`module`') then
                if nops(ListofCells[1]:-Edges[1][1]) = 3 then
                    map(c -> op(c:-Edges), ListofCells)
                else
                    map(c -> op(c:-Edges3D), ListofCells)
                fi;
            else
                ListofCells
            fi;
        end;

    	UniqueEdges := proc (Edges, epsilon := 0.1e-3)
    	    local UniqueEdgesHelper;
    	    UniqueEdgesHelper := proc (Pts, epsilon := 0.1e-3)
    	        local distances, strata, stratum, i;
    	        if Pts = {} then
    	            return {}
    	        end if;
    	        distances := sort([seq([evalf(PointNorm(Pts[i][1]-Pts[1][1])), Pts[i]], i = 1 .. nops(Pts))]); #, (x, y) -> evalb(evalf(x[1]) < evalf(y[1])+epsilon));
    	        strata := [];
    	        stratum := {};
    	        for i from 2 to nops(distances) do
    	            if epsilon < distances[i][1]-distances[i-1][1] then
    	                strata := [op(strata), stratum];
    	                stratum := {distances[i][2]} else stratum := {distances[i][2], op(stratum)}
    	            end if
    	        end do;
    	        strata := [op(strata), stratum];
    	        if nops(strata) = 1 then
    	            return {Pts[1][2]} else return {Pts[1][2]} union foldl(`union`, {}, op(map(UniqueEdgesHelper, strata[2 .. -1], epsilon)))
    	        end if
    	    end proc;
    	    UniqueEdgesHelper(map(x -> [expand((x[1]+x[2])/2), x], Edges), epsilon)
    	end proc;

    	Cells := CellsOrEdges;
    	Longest := longest;
    	Ratio := `if`(longest = [], -1, system:-Ratios[longest[1]][longest[2]]);
    	Edges := UniqueEdges(EdgesFrom(CellsOrEdges));
    	System := system;
    	Vertices := {op(map(op, Edges))};

        Intersection := proc(e, f)
            local A, M, b, i,T,z;
            z := [seq(0, i=1..nops(e[1]))];
            if (simplify(e[1]-f[1])=z and simplify(e[2]-f[2])=z) or (simplify(e[1]-f[2])=z and simplify(e[2]-f[1])=z) then
                return -1
            end if;
            M := [];
            b := [];
            for i to nops(e[1]) do
                M := [op(M), [f[2][i]-f[1][i], e[1][i]-e[2][i]]];
                b := [op(b), e[1][i]-f[1][i]]
            end do;
            if LinearAlgebra:-Rank(Matrix(M)) = 1 then
                try 
                    T := sort([LinearAlgebra:-LinearSolve(LinearAlgebra:-Transpose(Matrix([e[2]-e[1]])), LinearAlgebra:-Transpose(Matrix([f[1]-e[1]])))[1][1],
                               LinearAlgebra:-LinearSolve(LinearAlgebra:-Transpose(Matrix([e[2]-e[1]])), LinearAlgebra:-Transpose(Matrix([f[2]-e[1]])))[1][1]]);
                catch "inconsistent":
                    return -1
                end try;

                if T[2] <= 0 or T[1] >= 1 then
                    return -1
                end if
            end if;
            try
                A := LinearAlgebra:-LinearSolve(Matrix(M), convert(b, Vector));
            catch "inconsistent":
                return -1;
            end try;

            if A[1] <= 0 or 1 <= A[1] or A[2] <= 0 or 1 <= A[2] then
                return -1
            end if;
            A[1]*(f[2]-f[1])+f[1]
        end proc;
        
        PointInLine := proc(p, e)
            local k;
            k := LinearAlgebra:-LinearSolve(LinearAlgebra:-Transpose(Matrix(e[2]-e[1])), convert(p-e[1], Vector));
            if 0 < k[1] and k[1] < 1 then
                return true
            end if;
            return false
        end proc;
        
        # TODO: Use a more intelligent algorithm here.
        FindIntersections := proc(E)
            local L, i, j, p;
            L := [];
            for i to nops(E) do
                for j from i+1 to nops(E) do
                    p := Intersection(E[i], E[j]);
                    if p <> -1 then
                        L := [op(L), p]
                    end if
                end do
            end do;
            return L
        end proc;
        
        SplitIntersections := proc(V, E)
            local L, p, e, EdgesToRemove, NewEdges;
            L := FindIntersections(E);
            NewEdges := {};
            EdgesToRemove := {};
            for p in L do
                for e in E do
                    if PointInLine(p, e) then
                        EdgesToRemove := {op(EdgesToRemove), e};
                        NewEdges := {op(NewEdges), {e[1], p}, {e[2], p}}
                    end if
                end do
            end do;
            return {op(L), op(V)}, {op(E), NewEdges} minus EdgesToRemove
        end proc;

        RemoveSelfIntersections := proc()
            Vertices, Edges := SplitIntersections(Vertices, Edges); 
    	end;
       
        end;
    end;

    System := proc(labels, colours, cosines, ratios, points)
    	module ()

    	local CheckAngles, CheckRatios, NumericSubset, NumericSubsetF, DistanceSquareToOrigin, LengthSquare, Length, PointNorm, Cosine, ZomeColour, VectorSubsetF, SatisfiableConfig, CompleteVectorsToBasis, StdBasis, CreateRotation, MatrixFromListOfVectors, RotMatrix;
    	export Labels, Colours, Cosines, Ratios, Points, ModulePrint, Draw, Construct, Display, AssignColours;

    	Labels := labels;
    	Colours := convert(colours,table);
    	Cosines := convert(cosines,table);
    	Ratios := convert(ratios,table);
        Points := convert(points,table);

    	DistanceSquareToOrigin := p -> add(p[i]^2, i = 1 .. nops(p));
    	LengthSquare := e -> DistanceSquareToOrigin(e[2]-e[1]);
    	Length := e -> sqrt(DistanceSquareToOrigin(e[2]-e[1]));
        PointNorm := p -> sqrt(add(p[i]^2, i=1..nops(p)));

        # Check if A is a subset of B, by checking that each element of A belongs to B
    	NumericSubset := proc (A, B)
    		local x;
    		for x in A do
    			if `not`(0 in {op(map(y -> simplify(x-y), B))}) then
    				return false
    			end;
    		end;
    		return true
    	end;

        # As NumericSubset, but with a floating point tolerance controlled by epsilon
    	NumericSubsetF := proc(A, B, epsilon := 0.1e-3)
    		local x;
    		for x in A do
    			select( y -> is(y < epsilon), map(y -> abs(evalf(x-y)), B));
    			if nops(%) = 0 then
    			    return false
    			end;
    		end do;
    		return true
    	end;


        # Compute the cosine between two vectors as <u,v>/|u||v|
    	Cosine := proc (pt1, pt2)
    		local u, v; u := convert(pt1, Vector);
    		v := convert(pt2, Vector);
    		(u . v)/sqrt((u . u)*v . v)
    	end proc;

        # Colour an edge. Format is [label, index in Ratio[label]]
    	ZomeColour := proc(ratio, epsilon := 0.1e-2)
    		local l, i;
    		for l in Labels do
    			for i to nops(Ratios[l]) do
    				if is(abs(ratio-Ratios[l][i]) < epsilon) then
    					return [l, i]
    				end;
    			end;
    		end;
    		print(ratio, "not found")
    	end;

        # Colour every edge in the given list
    	AssignColours := proc (Edges, ratio)
    		local ratiof, lengthsf, maxlengthf, lengths;
    		ratiof := evalf(ratio);
    		lengthsf := map(x -> evalf(LengthSquare(x)), Edges);
    		lengths := map(LengthSquare(x), Edges);
    		maxlengthf := max(lengthsf);
    		[seq([Edges[i], ZomeColour(ratiof*sqrt(evalf(LengthSquare(Edges[i]))/maxlengthf))], i = 1 .. nops(Edges))]
    	end proc;

        # Determine whether a set of lengths can be assigned colours so that the ratios work properly
    	CheckRatios := proc(lengthSet, epsilon := 0.1e-2)
    		local f, r, l, RatioList, ZomeLengthRatios;

    		l := max(lengthSet);
    		RatioList := [];

    		# Get a list of Zome ratios, by taking all the ratios and putting them in a set
            ZomeLengthRatios := {op(map(op, [entries(Ratios, nolist)]))};

    		if epsilon = 0 then
    			f := NumericSubset
    		else
    			f := (x,y) -> NumericSubsetF(x, y, epsilon)
    		end;

    		for r in ZomeLengthRatios do
    			if f(map(x -> r*x/l, lengthSet), ZomeLengthRatios) then
    				RatioList := [op(RatioList), r];
    			end;
    		end;
    		return RatioList
    	end;

        # Compute if some set of points is a subset of another.
        VectorSubsetF := proc(A, B, epsilon := 0.1e-3)
            local x;
            for x in A do
                select(y -> is(y < epsilon), map(y -> evalf(PointNorm(x-y)), B));
                if nops(%) = 0 then
                    return false
                end
            end do;
            return true
        end proc;

	    MatrixFromListOfVectors := List -> LinearAlgebra:-Transpose(Matrix(List));

        StdBasis := proc (n, i)
            convert([seq(`if`(j = i, 1, 0), j = 1 .. n)], Vector)
        end proc;

        CompleteVectorsToBasis := proc (vs) local B, B2, i;
            if nops(vs) = 1 then
                for i to nops(vs[1]) do
                    B := [vs[1]];
                    B := `if`(i = 1, B, [op(B), seq(convert(StdBasis(nops(vs[1]), j), list), j = 1 .. i-1)]);
                    B := `if`(i = nops(vs[1]), B, [op(B), seq(convert(StdBasis(nops(vs[1]), j), list), j = i+1 .. nops(vs[1]))]);
                    if LinearAlgebra:-Determinant(Matrix(B)) <> 0 then
                        return B
                    end if
                end do
            end if;
            B := CompleteVectorsToBasis(vs[1 .. -2]);
            B2 := B;
            B2[nops(vs)] := vs[-1];
            if LinearAlgebra:-Determinant(Matrix(B2)) = 0 then
                B[nops(vs)+1] := B[nops(vs)];
                B[nops(vs)] := vs[-1] else B[nops(vs)] := vs[-1]
            end if;
            return B
        end proc;

        # NB: this is different than the createrotation in Wythoff
	    CreateRotation := proc (v, w := NORTH)
    	    local dim, V, W;
	        V := [convert(v, list)];
	        dim := nops(v);
	        if w = NORTH then
	           W := [[1, seq(0, i = 1 .. dim-1)]]
	        else
	           W := [convert(w, list)]
	        end if;
            LinearAlgebra:-MatrixInverse(MatrixFromListOfVectors(LinearAlgebra:-GramSchmidt(map(x->convert(x,Vector), CompleteVectorsToBasis(W)), normalized))) .
                                         MatrixFromListOfVectors(LinearAlgebra:-GramSchmidt(map(x->convert(x,Vector), CompleteVectorsToBasis(V)), normalized));
	    end proc;

    	RotMatrix := proc(x, y, z, theta)
    	    [[(1-x^2)*C+x^2, -z*S-y*x*C+y*x, y*S-x*z*C+x*z],
             [z*S-y*x*C+y*x, (1-y^2)*C+y^2, -x*S-y*z*C+y*z],
    	     [-y*S-x*z*C+x*z, x*S-y*z*C+y*z, (1-z^2)*C+z^2]];
    	    Matrix(subs(C = cos(theta), S = sin(theta), %))
    	end proc;

        # Determine if through some orthogonal transformation, Pts can be made to look like a subset of the Zome ball.
        SatisfiableConfig := proc(Pts, epsilon := 0.1e-1)
            local A, u, v, w, y, c, B, theta, T, pts, l, Targets, i;
            if nops(Pts) < 2 then
                return true
            end if;
            y := evalf(Points[Pts[1][2]][1]);
            v := evalf(expand(Pts[1][1]/PointNorm(Pts[1][1])));
            u := evalf(expand(Pts[2][1]/PointNorm(Pts[2][1])));
            c := Pts[2][2];
            A := evalf(CreateRotation(v, y));
            i := 2:

            # Bad things happen when u and v are colinear; if it's possible to avoid this, let's do so.
            while PointNorm(u+v) < epsilon do
                i := i + 1:
                if i > nops(Pts) then
                    break;
                end if;
                u := evalf(expand(Pts[i][1]/PointNorm(Pts[i][1])));
                c := Pts[i][2];
            od;

            Targets := map(evalf, select(x -> is(evalf(abs(evalf(PointNorm(x-y)-PointNorm(u-v)))) < epsilon), Points[c]));
            for w in Targets do
                theta := LinearAlgebra:-VectorAngle(convert(evalf((Wythoff:-ProjectThroughPoint(y))(w)), Vector), evalf(convert((Wythoff:-ProjectThroughPoint(y))(convert(A . convert(u, Vector), list)), Vector)));
                B := evalf(RotMatrix(y[1], y[2], y[3], theta));
                if is(epsilon < evalf(PointNorm(convert(B . A . convert(u, Vector), list)-w))) then
                    theta := -theta;
                    B := evalf(RotMatrix(y[1], y[2], y[3], theta))
                end if;
                T := B . A;
                pts := table([seq(l = {}, l in Labels)]);
                for l in Labels do
                    select(x ->  is(x[2] = l), Pts);
                    map(x -> evalf(T . convert(evalf(x[1]), Vector)/PointNorm(x[1])), %);
                    pts[l] := {op(%), op(pts[l])}
                end do;
                if andmap(x -> VectorSubsetF(map(y -> convert(expand(y/PointNorm(y)), list), pts[x]), Points[x], epsilon), Labels) then
                    return true
                fi;
            od;
            return false
        end proc;


    	CheckAngles := proc(Edges, ratio, epsilon := 0.1e-2)
    		local u, v, w, ColouredEdges, C, lengths, e, i, j, t, Vertices, ss, cosines, Pts;
    		if epsilon = 0 then
    			ss := NumericSubset
    		else
    			ss := (x,y) -> NumericSubsetF(x, y, epsilon)
    		end:

    		Vertices := {op(map(op, Edges))};
    		lengths := map(Length, Edges);

    		t := table([seq(v = [], v in Vertices)]);
    		cosines := table([seq(seq([v,w]={}, v in Labels), w in Labels)]);
    		ColouredEdges := AssignColours(Edges, ratio);
    		for e in ColouredEdges do
    			t[e[1][1]] := [op(t[e[1][1]]), e];
    			t[e[1][2]] := [op(t[e[1][2]]), e];
    		end;

            for u in Vertices do
        	    for i from 1 to nops(t[u]) do
        	        for j from i+1 to nops(t[u]) do
        	    	    v := (t[u][i][1] minus {u})[1];
        			    w := (t[u][j][1] minus {u})[1];
        			    cosines[[t[u][i][2][1], t[u][j][2][1]]] := {op(cosines[[t[u][i][2][1], t[u][j][2][1]]]), Cosine(v-u, w-u)};
            	    end;
        	    end;
            end;
            if not(andmap(x -> ss(cosines[x], Cosines[x]), [seq(seq([v,w], v in Labels), w in Labels)])) then
                return false
            end;
            for u in Vertices do
                Pts := [];
                for e in t[u] do
                    Pts := [op(Pts), [(e[1] minus {u})[1]-u, e[2][1]]]
                end;
                if not(SatisfiableConfig(Pts)) then
                    return false
                end;
            end;
            return true;
    	end;

        # Construct a model from a set of edges.
    	Construct := proc(edges, epsilon := 0.1e-2)
    		local ratios, r, lengths, m;

    		lengths := select(x -> is(epsilon < x), {op(map(x -> if epsilon <> 0 then evalf(Length(x)) else Length(x) end if, edges))});
    		m := max(lengths);

    		ratios := CheckRatios(lengths, epsilon);
    		for r in ratios do
    			if CheckAngles(edges, r, epsilon) then
    				print("Constructed with ratio ", r, "; you can modify model ratio to save this value for later");
    				return AssignColours(edges, r);
    			end:
    		end:
    		if nops(ratios) = 0 then
    			print("No constructible ratios");
    		end:
    		print("Not Zome constructible");
    		return -1
    	end;

        # Draw the given model. Styles can be passed as an optional argument, which controls the style each line is
        # printed with on a per-colour basis (for example, styles = table([yellow = dot]) draws yellow lines dotted).
    	Draw := proc(model, styles := table(), epsilon := 0.1e-2)
    		local edges, ratio:
    		if model:-Ratio = -1 then
    			Construct(model:-Edges, epsilon):
    		else
    			AssignColours(model:-Edges, model:-Ratio):
    		fi:
    		[op(map( x -> plottools:-line(x[1][1], x[1][2], thickness = 2, color = Colours[x[2][1]], linestyle= `if`(styles[Colours[x[2][1]]] = evaln(styles[Colours[x[2][1]]]), solid, styles[Colours[x[2][1]]])), %))]
    	end;

        # As Draw, but it immediately displays it
    	Display := proc(inp, styles := table(), epsilon := 0.1e-2)
    		plots:-display(Draw(inp, styles, epsilon), axes=none, scaling=constrained);
    	end;

    	end;
    end;

    # We borrow this function from Wythoff
	UniqueEdges := proc (Edges, epsilon := 0.1e-3)
	    local UniqueEdgesHelper;
	    UniqueEdgesHelper := proc (Pts, epsilon := 0.1e-3)
	        local distances, strata, stratum, i;
	        if Pts = {} then
	            return {}
	        end if;
	        distances := sort([seq([evalf(PointNorm(Pts[i][1]-Pts[1][1])), Pts[i]], i = 1 .. nops(Pts))]); #, (x, y) -> evalb(evalf(x[1]) < evalf(y[1])+epsilon));
	        strata := [];
	        stratum := {};
	        for i from 2 to nops(distances) do
	            if epsilon < distances[i][1]-distances[i-1][1] then
	                strata := [op(strata), stratum];
	                stratum := {distances[i][2]} else stratum := {distances[i][2], op(stratum)}
	            end if
	        end do;
	        strata := [op(strata), stratum];
	        if nops(strata) = 1 then
	            return {Pts[1][2]} else return {Pts[1][2]} union foldl(`union`, {}, op(map(UniqueEdgesHelper, strata[2 .. -1], epsilon)))
	        end if
	    end proc;
	    UniqueEdgesHelper(map(x -> [expand((x[1]+x[2])/2), x], Edges), epsilon)
	end proc;
    
    # Construct the model and then assign colours; then count the number of pieces of each kind.
    # Output is in the form of a table
    PartsList := proc(M, S)
        local L, t, v;
        if M:-Ratio = -1 then
            S:-Construct(UniqueEdges(M:-Edges))
        else
            S:-AssignColours(UniqueEdges(M:-Edges), M:-Ratio)
        end if;
        L := %;
        t := table();
        for v in L do
            if t[v[2]] = evaln(t[v[2]]) then
                t[v[2]] := 1
            else
                t[v[2]] := t[v[2]]+1
            end if
        end do;
        t["Balls"] := nops(M:-Vertices);
        return convert(t, table)
    end proc;

    # Draw a model around the given cell c, showing all cells with center of mass within distance d of c's center of mass,
    # where the "distance" between x and y is f(x-y). f should at least satisfy f(x-y) = f(y-x) for all x,y for this to behave reasonably
    LocalDraw := proc(M, c, d, f := PointNorm)
        select(x -> is(simplify(f(x:-CenterOfMass3D-c:-CenterOfMass3D)-d) <= 0) and is(simplify(f(x:-CenterOfMass3D-c:-CenterOfMass3D)) <> 0), M:-Cells);
        plots:-display([op(RoughDraw(%)), op(M:-System:-Draw(Model(c, M:-System, M:-Longest)))], axes = none, scaling = constrained)
    end proc;

    # Draw part of a model.
    # Orders the cells by h(center of mass of cell), then prints all cells with h-value < h(top) as black
    # If fullLevel is true, it draws all cells with h(cell) = h(top) in Zome colour
    # otherwise, it just draws top in Zome colour, and does not draw other cells with h(cell) = h(top)
    # If a filter is provided, it further only prints cells for which filter(center of mass of cell) is true
    AlwaysTrue := proc(x)
        return true
    end;
    PartialDraw := proc(M, h, top, fullLevel := true, filter := AlwaysTrue)
        local ToDraw, upper;
        upper := h(top:-CenterOfMass3D);
        select(proc (x) options operator, arrow;
        is(simplify(h(x:-CenterOfMass3D)-upper) = 0) and filter(x:-CenterOfMass3D) end proc, M:-Cells);
        ToDraw := M:-System:-Draw(Model(`if`(fullLevel, %, top), M:-System, M:-Longest)); #Add this argument to get line styles: table([blue = dash, red = dashdot, "Goldenrod" = solid]));
        select(proc (x) options operator, arrow;
        is(simplify(h(x:-CenterOfMass3D)-upper) < 0) and filter(x:-CenterOfMass3D) end proc, M:-Cells);
        ToDraw := [op(ToDraw), op(RoughDraw(%))];
        plots:-display(ToDraw, axes = none, scaling = constrained)
    end proc;
end module;
