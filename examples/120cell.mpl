# Load libraries
read "Wythoff.mpl";
read "Zome.mpl";
read "IcoZome.mla";

with(Wythoff);
with(Zome);

# Preload the 120-cell
read "examples/data/C120.mla":

# This could also be generated as follows
# V120, E120 := Wythoffian("H", 4, [1]); <-- May take a while; it's a good idea to use the pregenerated H4.mla present in data. Pass H4 as the fourth, optional argument if you do this.
# C120 := CreateCells(E120, V120);
# ProjectCells(C120);

# Make a model out of the 120-cell data
# If you want to verify the 120-cell is Zome constructible in this projection, omit the last argument.
# We already know that it is though, so telling Maple that the longest edge is a B2 makes this step much faster.
C120Model := Model(C120, IcoZome, ["B", 2]);

# Get a parts list of this model
print(PartsList(C120Model, IcoZome));

# Let's draw some of the model
# Arg 1 is the model
# Arg 2 is the height function
# Arg 3 is the topmost cell to be drawn
# Arg 4 is an optional argument controlling whether all cells at the level of arg 3 should be drawn, or just arg 3.
# Arg 5 is an optional predicate used for filtering
# Compare the following
PointNorm := p -> sqrt(add(p[i]^2, i=1..nops(p))):
PartialDraw(C120Model, PointNorm, C120Model:-Cells[2], false);
PartialDraw(C120Model, PointNorm, C120Model:-Cells[2], true);
PartialDraw(C120Model, PointNorm, C120Model:-Cells[2], true, x-> is(x[3]>=0));

# To draw the model locally around a cell, use LocalDraw from Zome
# Arg 1 is the model
# Arg 2 is the center cell to draw around
# Arg 3 is the distance from this cell we should draw until
# Arg 4 is the distance function
LocalDraw(C120Model, C120[1], 0.5);
LocalDraw(C120Model, C120[1], 0.5, x -> x[3]);

# Finally, we can take a look at the Schlegel diagram, as it is nice looking.
ProjectCells(C120, StereographicProject(C120[-1][CenterOfMass]));
RoughDisplay(C120);
