read "Wythoff.mpl"; # For creating cells
read "Zome.mpl";	# For creating Zome models
read "IcoZome.mla"; # The typical icosahedral Zome system

with(Wythoff);
with(Zome);

# Generate vertices, edges, cells of a hypercube via the Wythoff construction,
#                         4
# [x] ---- [x] ----- [x] ----- []
Vertices, Edges := Wythoffian("B", 4, [4]);
Cells := CreateCells(Edges, Vertices);

# Using a stereographic projection just outside of the center of mass of a cell, we get the Schlegel diagram.
ProjectCells(Cells, StereographicProject(Cells[1][CenterOfMass]));

# Display projection
RoughDisplay(Cells);

# Create a model in the icosahedral Zome system
M := Model(Cells, IcoZome);

# Verify that this is not constructible in ZomeTool
IcoZome[Draw](M);

# Project cell-first through a cubic cell
ProjectCells(Cells, ProjectThroughPoint(Cells[1][CenterOfMass]));

# Display projection
RoughDisplay(Cells);

# Regenerate the model
M := Model(Cells, IcoZome);

# Print a Zome construction
IcoZome[Display](M);
